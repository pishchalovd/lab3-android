package course.labs.activitylab;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

	// string for logcat documentation
	private final static String TAG = "Lab-ActivityOne";


	// lifecycle counts
	private int ctrCreate = 0;
	private int ctrStart = 0;
	private int ctrResume = 0;
	private int ctrPause = 0;
	private int ctrStop = 0;
	private int ctrRestart = 0;
	private int ctrDestroy = 0;

	public static final String PREFS = "myPrefs";

	//Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
	// You will need to increment these variables' values when their corresponding lifecycle methods get called.

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_one);

		//Log cat print out
		Log.i(TAG, "onCreate called");

		readInfo();
		//update the appropriate count variable
		ctrCreate++;
		//update the view
		TextView txtView = (TextView) findViewById(R.id.create);
		String str = getString(R.string.onCreate) + " " + ctrCreate;
		txtView.setText(str);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_one, menu);
		return true;
	}

	// lifecycle callback overrides

	@Override
	public void onStart() {
		super.onStart();

		//Log cat print out
		Log.i(TAG, "onStart called");


		//update the appropriate count variable
		ctrStart++;
		//update the view
		TextView txtView = (TextView) findViewById(R.id.start);
		String str = getString(R.string.onStart) + " " + ctrStart;
		txtView.setText(str);

	}

	@Override
	public void onResume() {
		super.onResume();

		//Log cat print out
		Log.i(TAG, "onResume called");
		//update the appropriate count variable
		ctrResume++;
		//update the view
		TextView txtView = (TextView) findViewById(R.id.resume);
		String str = getString(R.string.onResume) + " " + ctrResume;
		txtView.setText(str);
	}


	@Override
	public void onPause() {
		super.onPause();

		//Log cat print out
		Log.i(TAG, "onPause called");

		//update the appropriate count variable
		ctrPause++;
		//update the view
		TextView txtView = (TextView) findViewById(R.id.pause);
		String str = getString(R.string.onPause) + " " + ctrPause;
		txtView.setText(str);
	}



	@Override
	public void onStop() {
		super.onStop();

		//Log cat print out
		Log.i(TAG, "onStop called");

		//update the appropriate count variable
		ctrStop++;
		//update the view
		TextView txtView = (TextView) findViewById(R.id.stop);
		String str = getString(R.string.onStop) + " " + ctrStop;
		txtView.setText(str);

	}

	@Override
	public void onRestart() {
		super.onRestart();

		//Log cat print out
		Log.i(TAG, "onRestart called");

		//update the appropriate count variable
		ctrRestart++;
		//update the view
		TextView txtView = (TextView) findViewById(R.id.restart);
		String str = getString(R.string.onRestart) + " " + ctrRestart;
		txtView.setText(str);
			}

	@Override
	public void onDestroy() {
		super.onDestroy();

		//Log cat print out
		Log.i(TAG, "onDestroy called");

		//update the appropriate count variable
		ctrDestroy++;
		//update the view
		TextView txtView = (TextView) findViewById(R.id.destroy);
		String str = getString(R.string.onDestroy) + " " + ctrDestroy;
		txtView.setText(str);
		saveInfo();

	}

/*Phase B staff
	@Override
	public void onSaveInstanceState(Bundle outstate) {

		super.onSaveInstanceState(outstate);
		Log.i(TAG, "onSaveInstanceState()");

		// write to the Bundle using put... methods
		outstate.putInt("ctrCreate", ctrCreate);
		outstate.putInt("ctrStart", ctrStart);
		outstate.putInt("ctrResume", ctrResume);
		outstate.putInt("ctrPause", ctrPause);
		outstate.putInt("ctrStop", ctrStop);
		outstate.putInt("ctrRestart", ctrRestart);
		outstate.putInt("ctrDestroy", ctrDestroy);
	}

	public void onRestoreInstanceState(Bundle instate) {

		super.onSaveInstanceState(instate);
		Log.i(TAG, "onRestoreInstanceState()");
		if (instate != null) {
			// Restore value of members from saved state
			ctrCreate = instate.getInt("ctrCreate");
			ctrStart = instate.getInt("ctrStart");
			ctrResume = instate.getInt("ctrResume");
			ctrPause = instate.getInt("ctrPause");
			ctrStop = instate.getInt("ctrStop");
			ctrRestart = instate.getInt("ctrRestart");
			ctrDestroy = instate.getInt("ctrDestroy");


		}
	}
*/
	public void launchActivityTwo(View view) {
		// This function launches Activity Two.
		// Hint: use Context’s startActivity() method.
		startActivity(new Intent(this, ActivityTwo.class));


	}

	public void saveInfo() {


		SharedPreferences prefs = getSharedPreferences(PREFS, MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt("ctrCreate", ctrCreate);
		editor.putInt("ctrStart", ctrStart);
		editor.putInt("ctrResume", ctrResume);
		editor.putInt("ctrPause", ctrPause);
		editor.putInt("ctrStop", ctrStop);
		editor.putInt("ctrRestart", ctrRestart);
		editor.putInt("ctrDestroy", ctrDestroy);

				//commit the changes
		editor.commit();
		Log.i(TAG, "saveInfo()");
	}

	//
	public void readInfo() {

		Log.i(TAG, "readInfo()");
		SharedPreferences prefs = getSharedPreferences(PREFS, MODE_PRIVATE);

		ctrCreate = prefs.getInt("ctrCreate", 0);
		ctrStart = prefs.getInt("ctrStart", 0);
		ctrResume = prefs.getInt("ctrResume", 0);
		ctrPause = prefs.getInt("ctrPause", 0);
		ctrStop = prefs.getInt("ctrStop", 0);
		ctrRestart = prefs.getInt("ctrRestart", 0);
		ctrDestroy = prefs.getInt("ctrDestroy", 0);

	}


	}

